
import com.atlassian.bux.{AIDLoginHelper, ConfigHelper}
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class GABPerformanceTest extends Simulation {

  ConfigHelper.initialise()

  val httpProtocol = http
    .baseURL("https://nik-jun13.jira-dev.com")
    .contentTypeHeader("application/json")

  // Replace your 'studio.crowd.tokenkey' here
  val headers = Map(
    "Referer" -> "https://nik-jun13.jira-dev.com/admin/billing/overview",
    "Cookie"  -> "studio.crowd.tokenkey=<< Your token key >>"
  )

  val billEstimateScenario = scenario("GAB Pricing")
    .exec(http("GAB_Pricing")
      .get("/admin/rest/billing/api/instance/pricing")
      .headers(headers))

  val billingDetailsScenario = scenario("GAB Customer Details")
    .exec(http("GAB_Customer-details")
      .get("/admin/rest/billing/api/instance/customer-details")
      .headers(headers))

  setUp(
    billEstimateScenario.inject(rampUsersPerSec (1) to (ConfigHelper.totalUsers()) during (ConfigHelper.rampUpTimeInSec() seconds),
      constantUsersPerSec (ConfigHelper.totalUsers()) during (ConfigHelper.runTimeInSec() seconds)),

    billingDetailsScenario.inject(rampUsersPerSec (1) to (ConfigHelper.totalUsers()) during (ConfigHelper.rampUpTimeInSec() seconds),
      constantUsersPerSec (ConfigHelper.totalUsers()) during (ConfigHelper.runTimeInSec() seconds))
  ).protocols(httpProtocol)
}