package com.atlassian.bux;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AIDLoginHelper {

    public String getCookie() throws Exception{
        String url = ConfigHelper.aidUrl() + "/verifyCredentials";

        HttpPost post = new HttpPost(url);
        post.addHeader("Content-Type","application/json");

        DefaultHttpClient client = new DefaultHttpClient();

        StringEntity entity = new StringEntity("{\"username\" : \""+
                ConfigHelper.userName()+"\",\"password\" : \""+ConfigHelper.password()+"\"}");

        post.setEntity(entity);

        HttpResponse response = client.execute(post);
        System.out.println("AID Login status " +response.getStatusLine());

        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(new Scanner(response.getEntity().getContent()).next());


        HttpGet get = new HttpGet(ConfigHelper.idUrl() + "/login?token="+obj.get("token"));
        get.addHeader("Content-Type","application/json");

        response = client.execute(get);
        System.out.println("ID platform login status " +response.getStatusLine());

        List<Cookie> atlToken = client.getCookieStore().getCookies().stream().
                filter(c -> c.getName().contains("cloud.session.token.stg")).collect(Collectors.<Cookie>toList());
        return atlToken.get(0).getName()+"="+atlToken.get(0).getValue();

    }
}
