package com.atlassian.bux;

import org.apache.commons.configuration.CompositeConfiguration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class ConfigHelper {
    private static String aidUrl;
    private static String idUrl;
    private static List<String> instances;
    private static String userName;
    private static String password;
    private static int totalUsers;
    private static int rampUpTimeInSec;
    private static int runTimeInSec;


    public static void initialise() throws ConfigurationException {
        String confFile = "src/main/resources/perf-test.conf";
        CompositeConfiguration config = new CompositeConfiguration();
        config.addConfiguration(new PropertiesConfiguration(new File(confFile)));
        aidUrl = config.getString("aidUrl");
        idUrl = config.getString("idUrl");
        instances = Arrays.asList(config.getStringArray("instances"));
        userName = config.getString("userName");
        password = config.getString("password");
        totalUsers = config.getInt("totalUsers");
        rampUpTimeInSec = config.getInt("rampUpTimeInSec");
        runTimeInSec = config.getInt("runTimeInSec");
    }

    public static String aidUrl() {
        return aidUrl;
    }

    public static String idUrl() {
        return idUrl;
    }

    public static List<String> instances() {
        return instances;
    }

    public static String userName() {
        return userName;
    }

    public static String password() {
        return password;
    }

    public static int totalUsers() {
        return totalUsers;
    }

    public static int rampUpTimeInSec() {
        return rampUpTimeInSec;
    }

    public static int runTimeInSec() {
        return runTimeInSec;
    }
}
