# Overview #
This repo contains few scripts for doing performance testing of BUX API using Gatling.


# How to run #

Configure the `perf-test.conf` then
```
#!Java

mvn clean install -Dgatling.simulationClass=BUXPerformanceTest

OR

mvn clean install -Dgatling.simulationClass=GABPerformanceTest


```